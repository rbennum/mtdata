<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
if (!isset($_SESSION))
{
	session_start();	
}

Route::get('/', function(){ 
	return 'keren';
});

Route::group(['prefix' => 'admin'], function()
{
	Route::get('/', ['as' => 'admin.home' , 'uses' => function(){ return View::make('admin.home'); }]); // membuka halaman views/admin/home.blade.php
	include('routes_facebook.php');
	include('routes_twitter.php');
	include('routes_youtube.php');
	include('routes_advanced.php');
});