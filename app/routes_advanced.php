<?php

Route::group(['prefix' => 'advanced'], function()
{
	// Route::get('/', function(){return 'Keren twitter';});
	// Route::get('/home', array('as' => 'admin.twitter.home', 'uses' => 'TwitterController@search'));
	// Route::post('/postData', array('uses' => 'TwitterController@tweetSearch'));
	Route::get('/advanced-search', array('as' => 'admin.advanced.home', 'uses' => 'AdvancedController@home'));
	Route::post('/post-advanced', array('uses' => 'AdvancedController@postAdvanced'));
});