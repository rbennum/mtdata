<?php

Route::group(['prefix' => 'twitter'], function()
{
	Route::get('/', function(){return 'Keren twitter';});
	Route::get('/home', array('as' => 'admin.twitter.home', 'uses' => 'TwitterController@search'));
	Route::post('/postData', array('uses' => 'TwitterController@tweetSearch'));
});