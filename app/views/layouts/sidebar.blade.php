<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
		

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
			<li class="{{ (Request::is('*facebook') ? 'active' : '') }}">
				<a href="#">
					<i class="fa fa-circle-o fa-fw"></i>Facebook<span class="fa arrow"></span> 
				</a>
				<ul class="treeview-menu">
					<li><a href="{{ route('admin.facebook.search.user') }}">User</a></li>
					<li><a href="{{ route('admin.facebook.page.posts') }}">Page Posts</a></li>
					<li><a href="{{ route('admin.facebook.page.insights') }}">Page Insights</a></li>
				</ul>
			</li>
			<li {{ (Request::is('*twitter') ? 'class="active"' : '') }}>
				<a href="{{ route('admin.twitter.home') }}"><i class="fa fa-circle-o fa-fw"></i>Twitter</a>
				<!-- /.nav-second-level -->
			</li>
			<li {{ (Request::is('*youtube') ? 'class="active"' : '') }}>
				<a href="{{ route('admin.youtube.home') }}"><i class="fa fa-circle-o fa-fw"></i>Youtube</a>
			</li>
			<li {{ (Request::is('*advanced') ? 'class="active"' : '') }}>
				<a href="{{ route('admin.advanced.home') }}"><i class="fa fa-circle-o fa-fw"></i>Advanced Search</a>
			</li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>