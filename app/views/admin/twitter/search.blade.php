@extends('layouts.core')

@section('content')
	{{ Form::open(['url'=>'admin/twitter/postData','method'=>'POST']) }}
		<div class="input-group">
			{{ Form::text('keyword',$keyword,array('class'=>'form-control', 'placeholder' => 'Search...')) }}
			<span class="input-group-btn">
				{{ Form::button('<i class="fa fa-search"></i>', array('class'=>'btn btn-default', 'id'=>'submitButton', 'type'=>'submit')) }}
			</span>
		</div>
	{{ Form::close() }}
	@if ($data != null)
		<br />
		<table id="searchTable" class="table table-bordered table-striped">
			<thead>
				<th>No</th>
				<th>ID</th>
				<th>Name</th>
				<th>Tweet</th>
				<th>Created At</th>
			</thead>
			<tbody>
				<?php $i = 1; ?>
				@foreach ($data as $row)
				<tr>
					<td>{{$i}}</td>
					<td><a href="{{ 'https://twitter.com/'.$row->user->screen_name.'/status/'.$row->id_str }}">{{ $row->user->screen_name }}</a></td>
					<td>{{ $row->user->name }}</td>
					<td>{{ $row->text }}</td>
					<?php
						$oldDate = $row->created_at;
						date_default_timezone_set('Asia/Jakarta');
						$newDate = date('d-m-Y H:i:s', strtotime($oldDate));
					?>
					<td>{{ $newDate }}</td>
				<?php $i++?>
				</tr>
			@endforeach
		   </tbody>
		</table>
	@endif
@stop
@section('jscript')
<script type="text/javascript">
	$(document).ready(function() 
	{ 
		$('#searchTable').DataTable({
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": false,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false
		});
	});
</script>
@stop