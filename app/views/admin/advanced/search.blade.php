@extends('layouts.core')

@section('content')
	{{ Form::open(['url'=>'admin/youtube/postData','method'=>'POST']) }}
		<div class="input-group">
			{{ Form::text('keyword',$keyword,array('class'=>'form-control', 'placeholder' => 'Search...')) }}
			<span class="input-group-btn">
				{{ Form::button('<i class="fa fa-search"></i>', array('class'=>'btn btn-default', 'id'=>'submitButton', 'type'=>'submit')) }}
			</span>
		</div>
	{{ Form::close() }}
	@if ($data != null)
		<br />
		<div class="panel panel-primary">
			<div class="panel-heading">Facebook</div>
			<table class="table table-bordered table-striped">
				<thead>
					<th>No</th>
					<th>Channel Title</th>
					<th>Kind</th>
					<th>Title</th>
					<th>Description</th>
					<th>Published At</th>
				</thead>
			</table>
		</div>
		<br />
		<div class="panel panel-info">
			<div class="panel-heading">Twitter</div>
			<table class="table table-bordered table-striped">
				<thead>
					<th>No</th>
					<th>Channel Title</th>
					<th>Kind</th>
					<th>Title</th>
					<th>Description</th>
					<th>Published At</th>
				</thead>
			</table>
		</div>
		<br />
		<div class="panel panel-danger">
			<div class="panel-heading">YouTube</div>
			<table class="table table-bordered table-striped">
				<thead>
					<th>No</th>
					<th>Channel Title</th>
					<th>Kind</th>
					<th>Title</th>
					<th>Description</th>
					<th>Published At</th>
				</thead>
			</table>
		</div>
	@endif
@stop