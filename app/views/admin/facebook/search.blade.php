@extends('layouts.core')

@section('content')
<p>Search "user" on Facebook <!--{{ (isset($type)) ? $type : ''; }}--></p>
{{ Form::open(['id' =>  'searchForm', 'method'=>'POST']) }}
<div class="input-group">
	{{ Form::text('keyword', $keyword = "", array('class'=>'form-control', 'id' => 'keywordText', 'placeholder' => 'Search...')) }}
	<span class="input-group-btn">
		{{ Form::button('<i class="fa fa-search"></i>', array('class'=>'btn btn-default', 'id'=>'submitButton'/*, 'type'=>'submit'*/)) }}
	</span>
</div>
{{ Form::close() }}
<br/>
<div id='searchResult'></div>
@stop
@section('jscript')
<script type="text/javascript">
	$(document).ready(function() 
	{ 
		$("#submitButton").click(function( event )
		{
			event.preventDefault();
			var type = "<?php echo $type; ?>";
			doSearchUserAjax();
		});  
		
		$("#keywordText").keypress(function (e) 
		{
			if (e.keyCode == 13) 
			{
				e.preventDefault();
				var type = "<?php echo $type; ?>";
				doSearchUserAjax();
			}
		});
	});
	
	function doSearchUserAjax()
	{
		var searchURL = "{{route('admin.facebook.search.user.post')}}";	
		console.log(searchURL);
		
		$.ajax({
			url: searchURL, //'http://myserver.dev/myAjaxCallURI',
			type: 'post',
			data: $('#searchForm').serialize(), // data dari form "searchForm" diubah ke dalam bentuk JSON
			dataType: 'json',
			success: function( _response ){ // berhasil update data kecenderungan
				console.log('keren ajax');
				console.log(_response); // variabel response-nya sudah dalam json
				//document.getElementById('searchResult').innerHTML = createTable(_response.caption, _response.data);
				$('#searchResult').html(createTable(_response.caption, _response.data));
				$('#searchTable').DataTable({
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": false,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false
				});
			},
			error: function( _response ){ // kasus ajaxnya error
				console.log('error ajax');
				console.log(_response);
			}
		});
	}
	
	function createTable(caption, data)
	{
		return ''
			+	 '<div class="box-body table-responsive no-padding">'
			+		'<table id="searchTable" class="table table-bordered table-striped">'
			//+			'<caption>' + caption + '</caption>'
			//+			createVisualizeButton()
			+			'<thead>'
			+				'<th>No</th>'
			+				'<th>ID</th>'
			+				'<th>Name</th>'
			+			'</thead>'
			+			'<tbody>'
			+	 			createTR(data)
			+			'<tbody>'
			+		'</table>'
			+ 	'</div>';
	}
	
	function createVisualizeButton()
	{
		return '<button type="button" id="visualizeButton" onclick="onVisualizeButton()">Visualize</button>';
	}
	
	function createTR(data)
	{
		var i = 1;
		var result = '';
		console.log(data);
		for (var rowIndex in data) {
			result += '' 
			+ 			'<tr>'
			+				'<td>' + i + '</td>'
			+ 				createTD(data[rowIndex])
			+			'</tr>'
			+ '';
			
			++i;
		}
		
		return result;
	}
	
	function createTD(row)
	{
		var result = '';
		for (var cellIndex in row) {
			var wew = "<a href='" + row[cellIndex] + "'>" + row[cellIndex] + "</a>"; 
			result += ''
			+				'<td>'+ ((cellIndex == 0) ? wew : row[cellIndex]) + '</td>'
			+ '';
		}
		return result;
	}
</script>
@stop