@extends('layouts.core')

@section('content')
<p>Search "page" on Facebook <!--{{ (isset($type)) ? $type : ''; }}--></p>
{{ Form::open(['id' =>  'searchForm', 'method'=>'POST']) }}
<div class="input-group">
	{{ Form::text('keyword', $keyword = "", array('class'=>'form-control', 'id' => 'keywordText', 'placeholder' => 'Search...')) }}
	<span class="input-group-btn">
		{{ Form::button('<i class="fa fa-search"></i>', array('class'=>'btn btn-default', 'id'=>'submitButton'/*, 'type'=>'submit'*/)) }}
	</span>
</div>
{{ Form::close() }}
<br/>
<div id='searchResult'></div>
@stop
@section('jscript')
<script type="text/javascript">
	$(document).ready(function() 
	{ 
		$("#submitButton").click(function( event )
		{
			event.preventDefault();
			var type = "<?php echo $type; ?>";
			if (type === 'posts')
			{
				doSearchPostsAjax();
			}
			else if (type === 'insights')
			{
				doSearchInsightsAjax();
			}
		});  
		
		$("#keywordText").keypress(function (e) 
		{
			if (e.keyCode == 13) 
			{
				e.preventDefault();
				var type = "<?php echo $type; ?>";
				if (type === 'posts')
				{
					doSearchPostsAjax();
				}
				else if (type === 'insights')
				{
					doSearchInsightsAjax();
				}
			}
		});
	});
	
	function doSearchPostsAjax()
	{
		var searchURL = "{{route('admin.facebook.page.posts.search')}}";	
		console.log(searchURL);
		
		$.ajax({
			url: searchURL, //'http://myserver.dev/myAjaxCallURI',
			type: 'post',
			data: $('#searchForm').serialize(), // data dari form "searchForm" diubah ke dalam bentuk JSON
			dataType: 'json',
			success: function( _response ){ // berhasil update data kecenderungan
				console.log('keren ajax');
				console.log(_response); // variabel response-nya sudah dalam json
				//document.getElementById('searchResult').innerHTML = createTable(_response.caption, _response.data);
				$('#searchResult').html(createTable(_response.caption, _response.data));
				$('#searchTable').DataTable({
					"bPaginate": true,
					"bLengthChange": false,
					"bFilter": false,
					"bSort": true,
					"bInfo": true,
					"bAutoWidth": false
				});
			},
			error: function( _response ){ // kasus ajaxnya error
				console.log('error ajax');
				console.log(_response);
			}
		});
	}
	
	function doSearchInsightsAjax()
	{
		var searchURL = "{{route('admin.facebook.page.insights.search')}}";	
		console.log(searchURL);
		
		$.ajax({
			url: searchURL, //'http://myserver.dev/myAjaxCallURI',
			type: 'post',
			data: $('#searchForm').serialize(), // data dari form "searchForm" diubah ke dalam bentuk JSON
			dataType: 'json',
			success: function( _response ){ // berhasil update data kecenderungan
				console.log('keren ajax');
				console.log(_response); // variabel response-nya sudah dalam json
				createChart(_response.caption, _response.data);		
			},
			error: function( _response ){ // kasus ajaxnya error
				console.log('error ajax');
				console.log(_response);
			}
		});
	}
	
	function createTable(caption, data)
	{
		return ''
			+	 '<div class="box-body table-responsive no-padding">'
			+		'<table id="searchTable" class="table table-bordered table-striped">'
			//+			'<caption>' + caption + '</caption>'
			//+			createVisualizeButton()
			+			'<thead>'
			+				'<th>No</th>'
			+				'<th>ID</th>'
			+				'<th>Message</th>'
			+				'<th>Created Time</th>'
			+			'</thead>'
			+			'<tbody>'
			+	 			createTR(data)
			+			'<tbody>'
			+		'</table>'
			+ 	'</div>';
	}
	
	function createVisualizeButton()
	{
		return '<button type="button" id="visualizeButton" onclick="onVisualizeButton()">Visualize</button>';
	}
	
	function createTR(data)
	{
		var i = 1;
		var result = '';
		console.log(data);
		for (var rowIndex in data) {
			result += '' 
			+ 			'<tr>'
			+				'<td>' + i + '</td>'
			+ 				createTD(data[rowIndex])
			+			'</tr>'
			+ '';
			
			++i;
		}
		
		return result;
	}
	
	function createTD(row)
	{
		var result = '';
		for (var cellIndex in row) {
			var wew = "<a href='" + row[cellIndex] + "'>" + row[cellIndex] + "</a>"; 
			result += ''
			+				'<td>'+ ((cellIndex == 0) ? wew : row[cellIndex]) + '</td>'
			+ '';
		}
		return result;
	}
	
	function createChart(caption, data)
	{
		var theData = data.data[0];
		var fbDescription = theData.description;
		var fbId          = theData.id;
		var fbName        = theData.name;
		var fbPeriod      = theData.period;
		var fbTitle       = theData.title;
		var fbValues      = theData.values;
		var fbValue 		= fbValues[0].value;
		console.log(fbDescription);
		console.log(fbId);
		console.log(fbName);
		console.log(fbPeriod);
		console.log(fbTitle);
		console.log(fbValue);
		var highChartData = new Array();
		
		for(var key in fbValue){
			highChartData.push(new Array('Country Code: ' + key, fbValue[key]));
		}
		
		console.log(highChartData);
		
		$('#searchResult').highcharts({
			chart: {
				type: 'bar'
			},
			title: {
				text: fbTitle
			},
			subtitle: {
				text: fbDescription
			},
			xAxis: {
				type: 'Counts',
				labels: {
					rotation: 0,
					style: {
						fontSize: '13px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Countries'
				}
			},
			legend: {
				enabled: false
			},
			tooltip: {
				pointFormat: 'Count: <b>{point.y}</b>'
			},
			series: [{
				name: fbName, //'Jumlah Dukungan',
				data: highChartData,
				dataLabels: {
					enabled: true,
					rotation: 0,
					color: '#FFFFFF',
					align: 'right',
					format: '{point.y}', // one decimal
					y: 10, // 10 pixels down from the top
					style: {
						fontSize: '1px',
						fontFamily: 'Verdana, sans-serif'
					}
				}
			}]
		});
	}
</script>
@stop