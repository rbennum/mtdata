@extends('layouts.core')

@section('content')
	{{ Form::open(['url'=>'admin/youtube/postData','method'=>'POST']) }}
		<div class="input-group">
			{{ Form::text('keyword',$keyword,array('class'=>'form-control', 'placeholder' => 'Search...')) }}
			<span class="input-group-btn">
				{{ Form::button('<i class="fa fa-search"></i>', array('class'=>'btn btn-default', 'id'=>'submitButton', 'type'=>'submit')) }}
			</span>
		</div>
	{{ Form::close() }}
	@if ($data != null)
		<br />
		<table id="searchTable" class="table table-bordered table-striped">
			<thead>
				<th>No</th>
				<th>Channel Title</th>
				<th>Kind</th>
				<th>Title</th>
				<th>Description</th>
				<th>Published At</th>
			</thead>
			<tbody>
				<?php $i = 1; ?>
				@foreach ($data as $row)
				<tr>
					<td>{{$i}}</td>
					<td>{{ $row->snippet->channelTitle }}</td>
					<?php
						$kind = null;
						if ($row->id->kind == 'youtube#video')
						{
							$kind = 'Video';
						}elseif ($row->id->kind == 'youtube#playlist') {
							$kind = 'Playlist';
						}else{
							$kind = 'Channel';
						}
					?>
					<td>{{ $kind }}</td>
					<td><a href="{{ 'https://youtube.com/watch?v='.$row->id->videoId }}">{{ $row->snippet->title }}</a></td>
					<td>{{ $row->snippet->description }}</td>
					<td>{{ $row->snippet->publishedAt }}</td>
				<?php $i++?>
				</tr>
			@endforeach
		   </tbody>
		</table>
	@endif
@stop
@section('jscript')
<script type="text/javascript">
	$(document).ready(function() 
	{ 
		$('#searchTable').DataTable({
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": false,
			"bSort": true,
			"bInfo": true,
			"bAutoWidth": false
		});
	});
</script>
@stop