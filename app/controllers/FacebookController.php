<?php
use \Facebook\FacebookRequset;
class FacebookController extends HomeController 
{
	public function doLogin()
	{ 
		$fb = new Facebook\Facebook(Config::get('facebook'));
		$helper = $fb->getRedirectLoginHelper();
		$callback = url('/admin/facebook/login/callback');
		$permissions = ['public_profile', 
			'user_friends', 
			'email', 
			'user_about_me', 
			'user_actions.books', 
			'user_actions.fitness', 
			'user_actions.music', 
			'user_actions.news', 
			'user_actions.video', 
			/*'user_actions:{app_namespace}',*/
			'user_birthday', 
			'user_education_history', 
			'user_events', 
			'user_games_activity', 
			'user_hometown', 
			'user_likes', 
			'user_location', 
			'user_managed_groups', 
			'user_photos', 
			'user_posts', 
			'user_relationships', 
			'user_relationship_details', 
			'user_religion_politics', 
			'user_tagged_places', 
			'user_videos', 
			'user_website', 
			'user_work_history', 
			'read_custom_friendlists', 
			'read_insights', 
			'read_audience_network_insights', 
			'read_page_mailboxes', 
			'manage_pages', 
			'publish_pages', 
			'publish_actions', 
			'rsvp_event', 
			'pages_show_list', 
			'pages_manage_cta', 
			'ads_read', 
			'ads_management']; // optional
		$loginUrl = $helper->getLoginUrl($callback, $permissions);
		//dd($loginUrl);
		return Redirect::to((string)$loginUrl);
	}
	
	public function doLoginCallback()
	{
		$code = Input::get('code');
		$conf = Config::get('facebook');
		$fb = new Facebook\Facebook($conf);
		$helper = $fb->getRedirectLoginHelper();
		if ( !empty( $code ) ) {
			try {
				$accessToken = $helper->getAccessToken();
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
			  // When Graph returns an error
			  echo 'Graph returned an error: ' . $e->getMessage();
			  exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				  // When validation fails or other local issues
				  echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

			if (isset($accessToken)) {
			  // Logged in!
			  Session::put('code', (string) $code); 
			  Session::put('facebook_access_token', (string) $accessToken);
			  Session::put('default_access_token', (string) $conf['default_access_token']);
			  //$_SESSION['facebook_access_token'] = (string) $accessToken;

			  // Now you can redirect to another page and use the
			  // access token from $_SESSION['facebook_access_token']

			  //return Redirect::to(url('/admin'));
			  return Redirect::route('admin.home');
			  // return Redirect::route('admin.facebook.login.callback');
			} 
		}	
	}
	
	public function showDashboard($data = [])
	{
		/*
		https://www.facebook.com/search/top/?q=bom%20jakarta
		https://www.facebook.com/search/latest/?q=bom%20jakarta
		https://www.facebook.com/search/people/?q=bom%20jakarta
		https://www.facebook.com/search/photos/?q=bom%20jakarta
		https://www.facebook.com/search/videos/?q=bom%20jakarta
		https://www.facebook.com/search/pages/?q=bom%20jakarta
		https://www.facebook.com/search/places/?q=bom%20jakarta
		https://www.facebook.com/search/groups/?q=bom%20jakarta
		https://www.facebook.com/search/apps/?q=bom%20jakarta
		https://www.facebook.com/search/events/?q=bom%20jakarta
		*/
		$token = Session::get('facebook_access_token');
		if (isset($token)) // jika sudah login
		{
			if (isset($data))
			{
				if ($data['type'] == 'user')
				{
					return View::make('admin.facebook.search')
						->with('type', $data['type']);
				}
				else
				{
					return View::make('admin.facebook.page')
						->with('type', $data['type']);
				}
					
			}
		}
		else
		{
			return Redirect::route('admin.facebook.login');
		}
	}
	
	public function showSearchUserDashboard()
	{
		return FacebookController::showDashboard(['type' => 'user']);
	}
	
	public function showPagePostsDashboard()
	{
		return FacebookController::showDashboard(['type' => 'posts']);
	}
	
	public function showPageInsightsDashboard()
	{
		return FacebookController::showDashboard(['type' => 'insights']);
	}
	
	public function searchUser()
	{
		//http://stackoverflow.com/questions/10027805/how-to-see-get-all-wall-posts-of-a-specific-facebook-page
		$code = Session::get('code');
		$token = Session::get('facebook_access_token');
		$input = Input::all();
		$keyword = $input['keyword'];
		$parsedKeyword = str_replace(" ", "+", $keyword);
		$url = "https://graph.facebook.com/v2.5".
			"/search".
			"?q=".$parsedKeyword.
			"&type=user".
			//"&fields=id,name".
			"&access_token=".$token;
		
		$result = json_decode(file_get_contents($url));
		//$keyword = 'http://www.google.com';
		//dd($input);
		//dd($result);
		$resultData = $result->data;
		$new_data = [];
		foreach($resultData as $row)
		{ 	 	
			$id = '';
			$name = '';
			
			if(isset($row->id))
			{
				$id = $row->id;
			}
			
			if(isset($row->name))
			{
				$name = $row->name;
			}
			
			$new_row = [];
			$new_row = [$id, $name];
			$new_data[] = $new_row;
		}
		//dd($result);
		//dd($resultData);
		return Response::json(['caption' => $keyword, 'data' => $new_data]);
		/*
		return Response::json(['data' => Input::all()]);	;
		
		return View::make('admin.facebook.search')
			->with('type', '')
			->with('caption', $keyword)
			->with('data', $new_data);
		*/
	}
	
	public function searchPagePosts()
	{
		//http://stackoverflow.com/questions/10027805/how-to-see-get-all-wall-posts-of-a-specific-facebook-page
		$code = Session::get('code');
		$token = Session::get('facebook_access_token');
		$input = Input::all();
		$keyword = $input['keyword'];
		$parsedKeyword = str_replace(" ", "+", $keyword);
		$url = "https://graph.facebook.com/v2.5/"
			.$parsedKeyword.
			"/posts".
			"?access_token=".$token;
		$result = json_decode(file_get_contents($url));
		//$keyword = 'http://www.google.com';
		//dd($input);
		//dd($result);
		$resultData = $result->data;
		$new_data = [];
		foreach($resultData as $row)
		{ 	 	
			$id = '';
			$message = '';
			$created_time = '';
			
			if(isset($row->id))
			{
				$id = $row->id;
			}
			
			if(isset($row->message))
			{
				$message = $row->message;
			}
			
			if(isset($row->created_time))
			{
				$created_time = $row->created_time;
			}
			
			$new_row = [];
			$new_row = [$id, $message, $created_time];
			$new_data[] = $new_row;
		}
		//dd($result);
		//dd($resultData);
		return Response::json(['caption' => $keyword, 'data' => $new_data]);
		/*
		return Response::json(['data' => Input::all()]);	;
		
		return View::make('admin.facebook.search')
			->with('type', '')
			->with('caption', $keyword)
			->with('data', $new_data);
		*/
	}
	
	public function searchPageInsights()
	{
		//return Response::json(['caption' => $keyword, 'data' => 'keren insights']);
		//http://stackoverflow.com/questions/10027805/how-to-see-get-all-wall-posts-of-a-specific-facebook-page
		$code = Session::get('code');
		$token = Session::get('facebook_access_token');
		$input = Input::all();
		$keyword = $input['keyword'];
		$parsedKeyword = str_replace(" ", "+", $keyword);
		$url = "https://graph.facebook.com/v2.5/"
			.$parsedKeyword.
			"/insights".
			"?access_token=".$token;
		$result = json_decode(file_get_contents($url));
		//$keyword = 'http://www.google.com';
		//dd($input);
		//dd($result);
		$resultData = $result->data;
		$new_data = [];
		foreach($resultData as $row)
		{ 	 	
			$id = '';
			$message = '';
			$created_time = '';
			
			if(isset($row->id))
			{
				$id = $row->id;
			}
			
			if(isset($row->message))
			{
				$message = $row->message;
			}
			
			if(isset($row->created_time))
			{
				$created_time = $row->created_time;
			}
			
			$new_row = [];
			$new_row = [$id, $message, $created_time];
			$new_data[] = $new_row;
		}
		//dd($result);
		//dd($resultData);
		return Response::json(['caption' => $keyword, 'data' => $result]);
		/*
		return Response::json(['data' => Input::all()]);	;
		
		return View::make('admin.facebook.search')
			->with('type', '')
			->with('caption', $keyword)
			->with('data', $new_data);
		*/
	}
	
	public function redirectTo($addr)
	{
		$address = "http://www.facebook.com/".$addr;
		return Redirect::to($address);
	}
	
	public function showGroupMembers($id)
	{
		$code = Session::get('code');
		$token = Session::get('facebook_access_token');
		$limit = 1000;
		$offset=0;
		/*
		$url = 'https://graph.facebook.com/'
			.$id.
			'/members?access_token='
			.$token.
			'&fields=id,name&scope=email';
		*/
		
		// http://localhost:8081/laravel/public/facebook/groupmembers#_=		
		
		$url = "https://graph.facebook.com/v2.5/"
			.$id.
			"/members?fields=id,name".
			"&limit=".$limit.
			"&offset=".$offset.
			"&access_token=".$token;
		
		//$url = "";
		
		$result = json_decode(file_get_contents($url));
		//$paging = $result['paging'];
		//dd($result); //->paging);
		$ids = [];
		$names = [];
		if (count($result->paging) > 0)
		{
			$data = $result->data;
			foreach ($data as &$value) {
				$id = $value->id;
				$name = $value->name;
				//echo $id.', ';
				$ids[] = $id;
				$names[] = $name;
			}
			
			$paging = $result->paging;
			return $names;
			
			if (isset($paging->next))
			{
				$next = $paging->next;
				//return $next;
				//return $ids;
				//return $names;
				/*
				$result = json_decode(file_get_contents($url));
				//dd($result);
				if (count($result->paging) > 0)
				{
					$data = $result->data;
					foreach ($data as &$value) {
						$id = $value->id;
						$name = $value->name;
						//echo $id.', ';
						$coba[] = array('id' => $id, 'name' => $name);
					}
					
					$paging = $result->paging;
					$next = $paging->next;
				}
				*/
			}
			
			
		}
	}
}