<?php

use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterController extends Controller
{
	public function search()
	{
		$data = null;
		$keyword = null;

		if (Session::get('data') != null)
		{
			$data = Session::get('data');
			$data = $data->statuses;
			$keyword = Session::get('keyword');
		}

		return View::make('admin.twitter.search')->with('data', $data)->with('keyword', $keyword);
	}

	public function tweetSearch()
	{
		$keyword = Input::get('keyword');
		$parsedKeyword = str_replace(" ", "+", $keyword);

		$connection = new TwitterOAuth(Config::get('oauth_twitter.consumer'),Config::get('oauth_twitter.consumer_secret'),Config::get('oauth_twitter.access'),Config::get('oauth_twitter.access_secret'));

		$data = $connection->get('search/tweets', ['q' => $parsedKeyword, 'count' => 100, 'until' => '2016-01-22']);

		return Redirect::to('admin/twitter/home')->with('data', $data)->with('keyword', $keyword);
	}
}