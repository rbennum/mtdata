<?php

class YoutubeController extends Controller
{
	public function search()
	{
		$data = null;
		$keyword = null;

		if (Session::get('data') != null)
		{
			$data = Session::get('data');
			$data = $data->items;
			$keyword = Session::get('keyword');
		}

		return View::make('admin.youtube.search')->with('data', $data)->with('keyword', $keyword);
	}

	public function listSearch()
	{
		$youtube = Config::get('youtube_key.key');

		$keyword = Input::get('keyword');
		$parsedKeyword = str_replace(" ", "+", $keyword);

		$url = 'https://www.googleapis.com/youtube/v3/search?key='.$youtube.'&part=snippet&publishedAfter=2016-01-22T00:00:00Z&type=video&maxResults=50&q='.$parsedKeyword;
		// $url = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyCH7D_FuWMTVBYQkJ0ZMHQfdYk4j7_PJbU&part=snippet&q=gordon ramsay&maxResult=10';

		$data = json_decode(file_get_contents($url));

		return Redirect::to('admin/youtube/home')->with('data', $data)->with('keyword', $keyword);
	}
}