<?php

class AdvancedController extends Controller 
{
	public function home()
	{
		$data = null;
		$keyword = null;

		if (Session::get('data') != null)
		{
			$data = Session::get('data');
			$data = $data->statuses;
			$keyword = Session::get('keyword');
		}

		return View::make('admin.advanced.search')->with('data', $data)->with('keyword', $keyword);
	}

	public function postAdvanced()
	{
		$keyword = Input::get('keyword');
		$parsedKeyword = str_replace(" ", "+", $keyword);

		$connection = new TwitterOAuth(Config::get('oauth_twitter.consumer'),Config::get('oauth_twitter.consumer_secret'),Config::get('oauth_twitter.access'),Config::get('oauth_twitter.access_secret'));

		$data = $connection->get('search/tweets', ['q' => $parsedKeyword, 'count' => 100]);

		return Redirect::to('admin/advanced/home')->with('data', $data)->with('keyword', $keyword);
	}
}