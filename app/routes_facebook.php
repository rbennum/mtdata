<?php
Route::group(['prefix' => 'facebook'], function()
{
	//Route::get('/', ['as' => 'admin.facebook.home', 'uses' => 'FacebookController@showDashboard']);
	Route::group(['prefix' => 'login'], function()
	{	
		Route::get('/',  ['as' => 'admin.facebook.login', 'uses' => 'FacebookController@doLogin']);
		Route::get('/callback',  ['as' => 'admin.facebook.login.callback', 'uses' => 'FacebookController@doLoginCallback']);
	});
	///*
	//Route::group(['prefix' => 'home'], function()
	//{	
		//Route::get('/', ['as' => 'admin.facebook.home', 'uses' => 'FacebookController@showDashboard']);
		Route::group(['prefix' => 'page'], function()
		{
			Route::get('/posts', ['as' => 'admin.facebook.page.posts', 'uses' => 'FacebookController@showPagePostsDashboard']);
			Route::post('/posts', ['as' => 'admin.facebook.page.posts.search', 'uses' => 'FacebookController@searchPagePosts']);
			Route::get('/insights', ['as' => 'admin.facebook.page.insights', 'uses' => 'FacebookController@showPageInsightsDashboard']);
			Route::post('/insights', ['as' => 'admin.facebook.page.insights.search', 'uses' => 'FacebookController@searchPageInsights']);
			Route::get('/{addr}', ['as' => 'admin.facebook.page.redirect', 'uses' => 'FacebookController@redirectTo']);
		});
	//});	
	//*/
	Route::group(['prefix' => 'search'], function()
	{
		Route::get('/user', ['as' => 'admin.facebook.search.user', 'uses' => 'FacebookController@showSearchUserDashboard']);
		Route::post('/user', ['as' => 'admin.facebook.search.user.post', 'uses' => 'FacebookController@searchUser']);
		Route::get('/{addr}', ['as' => 'admin.facebook.search.redirect', 'uses' => 'FacebookController@redirectTo']);
	});	
});

Route::get('facebook/login/popup',  function() { 
	
	return View::make('keren');
});

/*
Route::get('facebook/home', function()
{
	return View::make('home');
});
*/



// dapetin member id dari suatu grup
Route::get('facebook/groupmembers/{id}', 'FacebookController@showGroupMembers');
